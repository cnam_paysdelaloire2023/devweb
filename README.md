# devweb

# Page public du cours
https://gitlab.com/cnam_paysdelaloire2023/devweb

## Projet Loto
- https://fr.wikipedia.org/wiki/Loto

## Théorie
- MVC

## Prototype client
### Première approximation
1. Une page web HTML + JS + CSS qui présente un carton
2. Générer un nombre de carton choisi par l'utilisateur
   - Créer la classe ```Carton```. Elle possède un attribut de type ```Array```. A l'intstantiation celui-ci contient  15 éléments choisit aléatoirement dans l'intervalle [1-90]
   - Créer la fonction ```carton2htmltable``` qui prend en paramètre un objet de type carton et le convertit en un tableau html.



## Infrastructure et Environement de développement
- Web Statique + Interactif:
  - répertoire + navigatoire 
- Web dynamique
    - Server Stack [WAMP](https://www.wampserver.com/) + XAMP
    - Windows + Apache + Mysql + PHP
- Framework ReacJs

# Supports de cours
- [NFA016 : Développement web (1) : architecture du web et développement côté client](https://informatique.cnam.fr/fr/spip.php?rubrique111)
- [NFA017 : Développement web (2) : sites dynamiques et développement côté serveu](https://informatique.cnam.fr/fr/spip.php?article400)
- [W3school](https://www.w3schools.com/)
- [MDN](https://developer.mozilla.org/fr/)


## Déroulé du cours/ projet
1. Developpement web statique (côté client)
- HTML
- CSS
2. Developpement web interactif
- Javascript
3. Developpement web dynamique
- PHP
4. Framework modernes
- PHP
    - [Symphony](https://symfony.com/)
    - [Laravel](https://laravel.com/)
- Javascript
    - [ReactJS](https://fr.reactjs.org/)
    - [Vue.js](https://vuejs.org/)
    - [Node.js](https://nodejs.org/en/)

## Auditeurs

### Clément T
BTS SN
- Skills : 
PHP, Javascript, html, css, 
Symphony
- Prêt pour autre chose

### Alexandre L
BTS SN
- Skills :
PHP, Javascript, html, css
- Prêt pour autre chose

### Tylan T
BTS SN
- Skills :
PHP, Javascript, html, css
Symphony
- Prêt pour autre chose

### Anthony H
30 ans éltecrotech
- Présentation des fondamentaux
- Objectif site dynamique Front htlm/css/js + back php mysql

### Cassandra G
- Licence anglais, vente
- OpenClassRoom
- Skills : Rappel
PHP, Javascript, html, css
Vue.js
- Présentation des fondamentaux
- React ?

### Solenne R
BTS SN
-  Skills : 
Javascript, html, css, Vue.js
- Rappel PHP
- Découverte React
#### Tuto react
- https://ibaslogic.com/react-components-data-model/
- https://www.w3schools.com/REACT

### Quentin G
DUT
- Js ++,html css, node.js
- PHP un peu moins
- ASP
- React +