-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: hackactorga
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hackactorga`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hackactorga` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `hackactorga`;

--
-- Table structure for table `hackathon`
--

DROP TABLE IF EXISTS `hackathon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hackathon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dateHeureDebut` varchar(20) DEFAULT NULL,
  `dateHeureFin` varchar(20) DEFAULT NULL,
  `lieu` varchar(20) DEFAULT NULL,
  `ville` varchar(20) DEFAULT NULL,
  `theme` varchar(20) DEFAULT NULL,
  `affiche` varchar(20) DEFAULT NULL,
  `objectifs` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hackathon`
--

LOCK TABLES `hackathon` WRITE;
/*!40000 ALTER TABLE `hackathon` DISABLE KEYS */;
INSERT INTO `hackathon` VALUES (1,'03/05/2021 8:00','05/05/2021 20:00','Espace Plan','La Rochelle','InterFlora','BelleAffiche','site web'),(2,'12/11/2021 8:00','13/11/2021 20:00','Espace Courbe','Rochefort','Paris en ligne','AfficheBelle','application mobile');
/*!40000 ALTER TABLE `hackathon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jury`
--

DROP TABLE IF EXISTS `jury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jury` (
  `hackathon_id` int NOT NULL,
  `membrejury_id` int NOT NULL,
  PRIMARY KEY (`hackathon_id`,`membrejury_id`),
  KEY `membrejury_id` (`membrejury_id`),
  CONSTRAINT `jury_ibfk_1` FOREIGN KEY (`hackathon_id`) REFERENCES `hackathon` (`id`),
  CONSTRAINT `jury_ibfk_2` FOREIGN KEY (`membrejury_id`) REFERENCES `membrejury` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jury`
--

LOCK TABLES `jury` WRITE;
/*!40000 ALTER TABLE `jury` DISABLE KEYS */;
INSERT INTO `jury` VALUES (1,1),(1,2),(2,2),(2,3);
/*!40000 ALTER TABLE `jury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membrejury`
--

DROP TABLE IF EXISTS `membrejury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `membrejury` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `mel` varchar(20) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membrejury`
--

LOCK TABLES `membrejury` WRITE;
/*!40000 ALTER TABLE `membrejury` DISABLE KEYS */;
INSERT INTO `membrejury` VALUES (1,'dalton','joe','joe@dalton.com','0123456789'),(2,'lucky','luke','luke@lucky.com','9876543210'),(3,'jumper','joly','joly@jumper.com','7418529630');
/*!40000 ALTER TABLE `membrejury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisateur`
--

DROP TABLE IF EXISTS `organisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisateur` (
  `id` int NOT NULL AUTO_INCREMENT,
  `statut` varchar(20) DEFAULT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `siteWeb` varchar(20) DEFAULT NULL,
  `mel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisateur`
--

LOCK TABLES `organisateur` WRITE;
/*!40000 ALTER TABLE `organisateur` DISABLE KEYS */;
INSERT INTO `organisateur` VALUES (1,'statut0','pim','http://www.pim.com','pim@pim.com'),(3,'statut1','pam','http://www.pam.com','pam@pam.com'),(4,'statut2','poum','http://www.poum.com','poum@poum.com');
/*!40000 ALTER TABLE `organisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisateur_hackathon`
--

DROP TABLE IF EXISTS `organisateur_hackathon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisateur_hackathon` (
  `organisateur_id` int NOT NULL,
  `hackathon_id` int NOT NULL,
  PRIMARY KEY (`organisateur_id`,`hackathon_id`),
  KEY `hackathon_id` (`hackathon_id`),
  CONSTRAINT `organisateur_hackathon_ibfk_1` FOREIGN KEY (`organisateur_id`) REFERENCES `organisateur` (`id`),
  CONSTRAINT `organisateur_hackathon_ibfk_2` FOREIGN KEY (`hackathon_id`) REFERENCES `hackathon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisateur_hackathon`
--

LOCK TABLES `organisateur_hackathon` WRITE;
/*!40000 ALTER TABLE `organisateur_hackathon` DISABLE KEYS */;
INSERT INTO `organisateur_hackathon` VALUES (1,1),(3,1),(3,2),(4,2);
/*!40000 ALTER TABLE `organisateur_hackathon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projet`
--

DROP TABLE IF EXISTS `projet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hackathon_id` int NOT NULL,
  `description` varchar(20) DEFAULT NULL,
  `retenu` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`hackathon_id`),
  KEY `hackathon_id` (`hackathon_id`),
  CONSTRAINT `projet_ibfk_1` FOREIGN KEY (`hackathon_id`) REFERENCES `hackathon` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projet`
--

LOCK TABLES `projet` WRITE;
/*!40000 ALTER TABLE `projet` DISABLE KEYS */;
INSERT INTO `projet` VALUES (1,1,'proj0','oui'),(2,1,'proj1','non'),(3,2,'proj2','oui'),(4,2,'proj3','non');
/*!40000 ALTER TABLE `projet` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-14 14:53:55
