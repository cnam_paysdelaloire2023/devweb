console.log("Hello World!")

function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

class Carton {
    constructor(){
        this.tab = [];
        for (var i=0; i<15; i++)
        {
            var val2insert;
            val2insert = getRandomArbitrary(1, 90);
            while(val2insert in this.tab)
            {
                val2insert = getRandomArbitrary(1, 90);
            }
            this.tab.push(val2insert);
        }
    }
}

test =  new Carton();

for (var i=0; i<15; i++)
        {
            console.log(test.tab[i]);
        }

var carton = document.getElementById('carton2');

//carton.innerText = "Bar";

//carton.innerHTML = "<table><tr><td>42</td></tr></table>";

function carton2HTMLtable(objCarton)
{
    ret = "</br><table>";

    var elem = 0;

    for (var i=0; i<3; i++)
    {
        ret = ret + "<tr>";
        for (var j=0; j<9; j++)
        {
            if ((i+j)%2 == 0)
            {
                ret = ret + "<td>" + objCarton.tab[elem] + "</td>";
                elem ++ ;
            } else {
                ret = ret + "<td></td>";
            }
        }
        ret = ret + "</tr>";
    }

    ret =  ret + "<td>" + objCarton.tab[0] + "</td>";

    return ret + "</table>";
}

carton.innerHTML = carton2HTMLtable(test);