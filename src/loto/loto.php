<?php

declare(strict_types = 1);

class Carton
{
  public $tab;

   function __construct()
   {
     //print "Dans le constructeur de Carton\n";
     $this->tab = [];
     for ($i =0; $i < 15; $i++)
     {
       $val2insert = rand(1, 90);
       while (in_array($val2insert, $this->tab))
       {
         $val2insert = rand(1, 90);
       }
       array_push($this->tab, $val2insert);
     }
   }
}

$test = new Carton;

print("Valeurs carton </BR>");

for ($i = 0; $i<15; $i++)
{
  print(strval($test->tab[$i]) . "</BR>");
}
 

function carton2HTMLtable(Carton $objCarton) : string
{
 //return "Hello World!";
  $ret = "<style>
	  table, th, td {
	    border: 1px solid black;
	    border-collapse:collapse;
	    }
          td { width:42px; height:42px; }
	 </style><table>";

    $elem = 0;

    $positions = [];
    for($i=0; $i<15; $i++)
    {
      $pos = rand(0, 26);
      while(in_array($pos, $positions))
      {
	$pos = rand(0, 26);
      }
      array_push($positions, $pos);
    }

    print("Positions </BR>"); 
    for ($i=0; $i<15; $i++)
    {
      print(strval($positions[$i]) . "</BR>");
    } 

    for ($i=0; $i<3; $i++)
    {
        $ret = $ret . "<tr>";
	
        for ($j=0; $j<9; $j++)
        {
            if (in_array($i*9+$j,$positions))
            {
                $ret = $ret . "<td>" . strval($objCarton->tab[$elem]) . "</td>";
                $elem ++ ;
            } else {
                $ret = $ret . "<td></td>";
            }
        }
        $ret = $ret . "</tr>";
    }

    //$ret =  $ret . "<td>" . strval($objCarton->tab[0]) . "</td>";
    return $ret . "</table>";
}

print(carton2HTMLtable($test));


?>
