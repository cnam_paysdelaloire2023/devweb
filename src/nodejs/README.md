Nodejs
===

# Installation de Nodejs
- https://nodejs.org/fr/download

# Tutorial 
- https://www.w3schools.com/nodejs/
- https://learn.microsoft.com/en-us/visualstudio/javascript/tutorial-nodejs?view=vs-2022

# Hello worl nodejs
https://nodejs.org/en/docs/guides/getting-started-guide

# Initialisation du répetoire comme un projet nodejs
```
npm init
```
